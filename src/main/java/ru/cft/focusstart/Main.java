package ru.cft.focusstart;

import org.apache.commons.cli.*;
import ru.cft.focusstart.mergesort.MergeSort;

import java.io.*;
import java.util.Comparator;

public class Main {
    private static void printHelp(Options options) {
        final String commandLineSyntax = "java -jar sort-it.jar [-a | -d] -s | -i output_file input_files...";
        final PrintWriter writer = new PrintWriter(System.out);
        final HelpFormatter helpFormatter = new HelpFormatter();

        helpFormatter.printUsage(writer, 80, commandLineSyntax);
        helpFormatter.printWrapped(writer, 80, "options:");
        helpFormatter.printOptions(writer, 80, options, 3, 8);

        writer.flush();
    }

    private static Options initializeOptions() {
        OptionGroup order = new OptionGroup();
        order.addOption(new Option("a", "ascending",
                false, "Ascending sorting order (default)"));
        order.addOption(new Option("d", "descending",
                false, "Descending sorting order"));
        order.setRequired(false);


        OptionGroup type = new OptionGroup();
        type.addOption(new Option("i", "integer",
                false, "Sort integers"));
        type.addOption(new Option("s", "string",
                false, "Sort strings"));
        type.setRequired(true);

        Option help = new Option("h", "help", false, "Print help");
        help.setRequired(false);

        Options options = new Options();
        options.addOption(help);
        options.addOptionGroup(order);
        options.addOptionGroup(type);

        return options;
    }


    public static void main(String[] args) {
        DefaultParser parser = new DefaultParser();
        Options options = initializeOptions();

        try {
            CommandLine cl = parser.parse(options, args, true);
            String[] fileNames = cl.getArgs();

            Comparator<String> comparator;
            if (cl.hasOption('i')) {
                comparator = Comparator.comparingInt(Integer::parseInt);
            } else {
                comparator = String::compareTo;
            }

            if (cl.hasOption('d')) {
                comparator = comparator.reversed();
            }

            InputStream[] files = new FileInputStream[fileNames.length - 1];

            OutputStream out = new FileOutputStream(fileNames[0]);
            for (int i = 1; i < fileNames.length; i++) {
                files[i - 1] = new FileInputStream(fileNames[i]);
            }
            MergeSort.sort(comparator, out, files);

            out.close();
            for (InputStream file : files) {
                file.close();
            }

        } catch (ParseException e) {
            printHelp(options);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (NumberFormatException e) {
            System.out.print(e.getMessage() + ": data format is not correct.");
        }
    }
}
