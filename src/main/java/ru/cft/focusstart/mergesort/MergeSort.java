package ru.cft.focusstart.mergesort;

import java.io.*;
import java.util.Comparator;
import java.util.logging.Logger;

public class MergeSort {
    private final static Logger log = Logger.getLogger(MergeSort.class.getName());

    /*
     * Returns position of the minimum element in the array
     * or -1 if all elements are null
     */
    private static int findMin(String[] array, Comparator<String> comparator) {
        int minIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[minIndex] == null || array[i] != null &&
                    comparator.compare(array[minIndex], array[i]) > 0) {
                minIndex = i;
            }
        }
        if (array[minIndex] == null) return -1;
        return minIndex;
    }

    /*
     * Sorts the contents in given input streams
     * and outputs it to the output stream
     * using provided comparator.
     * As you can see the comparator is for String objects,
     * because the data read from input streams is being
     * interpreted as the sequence of lines separated by
     * the new line symbol ('\n').
     */
    public static void sort(Comparator<String> compare,
                            OutputStream dest, InputStream... src) throws IOException {
        BufferedReader[] sources = new BufferedReader[src.length];

        // Contains first elements from the input streams which aren't written to the output yet
        String[] data = new String[src.length];

        // initializing
        for (int i = 0; i < sources.length; i++) {
            sources[i] = new BufferedReader(new InputStreamReader(new BufferedInputStream(src[i])));
            String line = sources[i].readLine();
            data[i] = line;
        }

        BufferedWriter output = new BufferedWriter(new OutputStreamWriter(dest));

        String max = null; // Last element written to the output
        int minIndex; // Next element to be written to the output and its position

        while ((minIndex = findMin(data, compare)) != -1) {
            String min = data[minIndex];
            if (max != null && compare.compare(min, max) < 0) {
                log.warning("File #" + (minIndex + 1) + " is corrupted");
            } else {
                output.write(min + "\n");
                max = min;
            }

            data[minIndex] = sources[minIndex].readLine();
        }
        output.flush();
    }
}